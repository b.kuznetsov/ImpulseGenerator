# ImpulseGenerator
Simple firmware project for ATmega8.

# Description
Whole Port B used for impulses output.
Port D used for input from control switches as follows:
* PD0 - 10 impulses;
* PD1 - 20 impulses;
* PD2 - 50 impulses;
* PD3 - 100 impulses;
* PD4 - Start button.

# Work sequence
1. User sets mode (10, 20, 50 or 100 impulses)
2. User presses the Start button;
3. The microcontroller generates specified count of impulses with predefined 500 ms timeout.