/*
 * ImpulseGeneartor.c
 *
 * Created: 11.02.2017 17:58:56
 * Author : Boris Kuznetsov
 */ 

#define F_CPU 8000000UL

// Sleeping half-interval
#define HALF_INTERVAL 250U

// Predefined impulses counts
#define MODE_10 10U
#define MODE_20 20U
#define MODE_50 50U
#define MODE_100 100U

#include <stdbool.h>
#include <avr/io.h>
#include <util./delay.h>

void initPorts(void);
void genrateImpulses(int count);
bool isModeSwitch10Pressed(void);
bool isModeSwitch20Pressed(void);
bool isModeSwitch50Pressed(void);
bool isModeSwitch100Pressed(void);
bool isControlSwitchPressed(void);
int getImpulsesCount();
void switchOn(void);
void switchOff(void);
void wait(void);
void run(void);

int main(void)
{
	
	initPorts();
	run();	
	
	return 0;
}

/************************************************************************/
/* Main execution loop                                                  */
/************************************************************************/
void run() {

	int count = 0;

    while (1) 
    {
		
		count = getImpulsesCount();

		if (isControlSwitchPressed()) {
		
			genrateImpulses(count);
		}
    }
}

/************************************************************************/
/* Scans mode switches and returns selected mode                        */
/************************************************************************/
int getImpulsesCount() {

	if (isModeSwitch10Pressed()) {

		return MODE_10;
	}

	if (isModeSwitch20Pressed()) {

		return MODE_20;
	}

	if (isModeSwitch50Pressed()) {
		
		return MODE_50;
	}

	if (isModeSwitch100Pressed()) {

		return MODE_100;
	}

	return 0;
}

/************************************************************************/
/* Returns true if 10 impulses mode switch activated.                   */
/************************************************************************/
bool isModeSwitch10Pressed() {

	return ((PIND & 0b00000001) == 0);
}

/************************************************************************/
/* Returns true if 20 impulses mode switch activated.                   */
/************************************************************************/
bool isModeSwitch20Pressed() {

	return ((PIND & 0b00000010) == 0);
}

/************************************************************************/
/* Returns true if 50 impulses mode switch activated.                   */
/************************************************************************/
bool isModeSwitch50Pressed() {

	return ((PIND & 0b00000100) == 0);
}

/************************************************************************/
/* Returns true if 100 impulses mode switch activated.                  */
/************************************************************************/
bool isModeSwitch100Pressed() {

	return ((PIND & 0b00001000) == 0);
}

/************************************************************************/
/* Returns true if start switch activated.                              */
/************************************************************************/
bool isControlSwitchPressed() {

	return ((PIND & 0b00010000) == 0);
}

/************************************************************************/
/* Generate specified count of impulses.                                */
/************************************************************************/
void genrateImpulses(int count) {

	for (int iteration = 0; iteration < count; iteration++) {
		
		switchOn();
		wait();
		switchOff();
		wait();
	}
}

/************************************************************************/
/* Ports setup.                                                         */
/************************************************************************/
void initPorts() {

	// Port B - Output
	DDRB = 0xFF;

	// Port D - Input
	DDRD = 0x00;
	PORTD = 0xFF;
}

/************************************************************************/
/* Sets high level on the port B.                                       */
/************************************************************************/
void switchOn() {

	PORTB = 0xFF;
}

/************************************************************************/
/* Sets low level on the port B.                                        */
/************************************************************************/
void switchOff() {
	
	PORTB = 0x00;
}

/************************************************************************/
/* Waits predefined timeout.                                            */
/************************************************************************/
void wait() {

	_delay_ms(HALF_INTERVAL);
}